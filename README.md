# PyWallet
An open-source Bitcoin Wallet written with Python.

## How to?
You need Python3, bit and click to run this code on your device. Soon I will release a requirements.txt file. After installing it source code to your computer you can just type `python3 main.py --help`

## Why Bit?
Because *Bit* uses Bitcoin Core’s heavily optimized C library libsecp256k1 for all elliptic curve operations. It makes Bit one of the fastest Bitcoin libraries in Python.

## Donations

*  BTC: 16iqg5L9L2tKAkjTi4c5LZbMBqiXXnCUFM
*  BTC (Segwit): 3JKuutSyKdiaVHzVaS75CQ2K3amog3tFc5
