from bit.network import get_fee, get_fee_cached
from bit import SUPPORTED_CURRENCIES
from bit import Key, MultiSig
from bit import wif_to_key
import getpass
import click

@click.command()
@click.option('--generate', is_flag=True, help='Generates a single address key for you.')
@click.option('--multisig', is_flag=True, help='Generates a multi-sig wallet for you.')
@click.option('--sign', is_flag=True, help='Signs a Bitcoin Transaction.')
@click.option('--fee', is_flag=True, help='Fees for a transaction. normal or fast.')
@click.option('--send', is_flag=True, help='Creates and signs a transaction.')
@click.option('--balance', is_flag=True, help='Displays the balance of wallet.')
@click.option('--transactions', is_flag=True, help='Displays the transactions newest to oldest.')

def btcwallet(generate,multisig,sign,fee,send,balance,transactions):

    if balance:
        wifkey = getpass.getpass(prompt='Please enter your WIF: ', stream=None)
        key = Key(wifkey)
        print("You have:", key.balance, "BTC")

    # IDEA: Improving multi-sig feature by editing this part.
    if multisig:
        try:
            count = int(input("How many cosigners need to make a transaction? "))
        except:
            print("You must enter a number.")
            btcwallet()
        key1 = Key()
        key2 = Key()
        multisig = MultiSig(key1, {key1.public_key, key2.public_key}, count)
        print("Public Key for first key:",key1.address)
        print("Segwit Address for first key:",key1.segwit_address)
        print("WIF Private Key for first key:",key1.to_wif())
        print("Hex Private Key for first key:",key1.to_hex())
        print("Public Key for second key:",key2.address)
        print("Segwit Address for second key:",key2.segwit_address)
        print("WIF Private Key for second key:",key2.to_wif())
        print("Hex Private Key for second key:",key2.to_hex())
        print("Public Key for multisig key:",multisig.address)
        print("Segwit Address for multisig key:",multisig.segwit_address)
        if click.confirm("Do you want to save a backup?"):
            filename = input("Please enter a filename for backup, without extension. ")
            f = open(filename,"w+")
            print("Public Key for first key:",key1.address, file=f)
            print("Segwit Address for first key:",key1.segwit_address, file=f)
            print("WIF Private Key for first key:",key1.to_wif(), file=f)
            print("Hex Private Key for first key:",key1.to_hex(), file=f)
            print("Public Key for second key:",key2.address, file=f)
            print("Segwit Address for second key:",key2.segwit_address, file=f)
            print("WIF Private Key for second key:",key2.to_wif(), file=f)
            print("Hex Private Key for second key:",key2.to_hex(), file=f)
            print("Public Key for multisig key:",multisig.address, file=f)
            print("Segwit Address for multisig key:",multisig.segwit_address, file=f)

    if transactions:
        wifkey = getpass.getpass(prompt='Please enter your WIF: ', stream=None)
        key = Key(wifkey)
        print(key.get_transactions())

    if send:
        wifkey = getpass.getpass(prompt='Please enter your WIF: ', stream=None)
        key = Key(wifkey)
        print("You have:", key.balance, "BTC")
        print("You are going to make a transaction with public key:", key.address)
        if click.confirm('Do you want to see supported currencies?'):
            print(SUPPORTED_CURRENCIES)
        address = input("Please enter the address. ")
        currency = input("Please enter currency. ")
        amount = input("Please enter amount. ")
        transactionfee = input("Please enter fee amount in satoshis. ")
        key.create_transaction([(address, amount, currency)])

    if fee:
        print("Fees for normal speed transaction",get_fee(fast=False))
        print("Fees for fast transaction",get_fee(fast=True))

    if generate:
        key = Key()
        print("Public Key:",key.address)
        print("Segwit Address:",key.segwit_address)
        print("WIF Private Key:",key.to_wif())
        print("Hex Private Key:",key.to_hex())
        if click.confirm('Do you want to save a backup?'):
            filename = input("Please enter a filename for backup, without extension. ")
            f = open(filename,"w+")
            print("Public Key:",key.address, file=f)
            print("Segwit Address:",key.segwit_address, file=f)
            print("WIF Private Key:",key.to_wif(), file=f)
            print("Hex Private Key:",key.to_hex(), file=f)

if __name__ == '__main__':
    btcwallet()
